import unittest
import warnings

class Node:
    """A binary-tree node class to store 2d-points"""

    def __init__(self, point, left_node=None, right_node=None):
        """Class initialization

        Args:
            point (tuple) : Integer tuple with x and y coordinates
            left_node (Node, optional) : Node to attach as the left child
            right_node (Node, optional) : Node to attach as the right child
        """

        assert isinstance(point, tuple), 'Argument1 should be a tuple'
        self.point = point
        self.dims = len(self.point)
        self.left = left_node
        self.right = right_node

        if self.left is not None:
            assert isinstance(self.left, Node), 'Argument2 not a node instance'
            assert (self.left.dims == self.dims), \
                    'Required tuple of size {}. Got {}'.format(self.dims,
                            self.left.dims)

        if self.right is not None:
            assert isinstance(self.right, Node), 'Argument3 not a node instance'
            assert (self.right.dims == self.dims), \
                    'Required tuple of size {}, Got {}'.format(self.dims,
                            self.right.dims)

class KDTree:
    """ K-Dimensional Tree class"""

    def __init__(self, dims):
        """Class initialization

        Args:
            dims (integer): Number of dimensions of the tree
        """

        self.dims = dims

    def __insert(self, point, node, sDim):
        """Inserts the point at a suitable location from the given node

        Args:
            point (tuple) : Coordinates to insert to the tree
            node (Node) : Node to begin traversal
            sDim (Integer) : Splitting / cutting dimension of the input node

        Returns:
            Node after insertion
        """

        if node is None:
            node = Node(point)
        else:
            if point[sDim] < node.point[sDim]:
                node.left = self.__insert(point, node.left,
                        (sDim + 1) % self.dims)
            elif point[sDim] > node.point[sDim]:
                node.right = self.__insert(point, node.right,
                        (sDim + 1) % self.dims)
            else:
                warnings.warn('Duplicate point {}. Ignored.'.format(point))
        return node

    def __find_min(self, node, dim, sDim):
        """Finds the node with minimum value in the given dimension

        Args:
            node (Node) : Entry node to begin the search for the minimum
            dim  (Integer) : Dimension to search for the minimum
            sDim (Integer) : Splitting / cutting dimension of the input node

        Returns:
            Point (tuple) with minimum value in the given dimension
        """

        if node is None:
            return None
        elif dim == sDim:
            min_node = node.point
            if node.left:
                min_node = self.__find_min(node.left, dim,
                        (sDim + 1) % self.dims)

            return min_node

        else:
            min_node = None
            min_left = self.__find_min(node.left, dim, (sDim + 1) % self.dims)
            min_right = self.__find_min(node.right, dim, (sDim + 1) % self.dims)

            if min_left and min_right:
                if min_left[dim] < min_right[dim]:
                    min_node = min_left
                else:
                    min_node = min_right
            else:
                min_node = min_left or min_right

            if min_node:
                if node.point[dim] < min_node[dim]:
                    min_node = node.point
                return min_node
            else:
                return node.point

    def __delete(self, point, node, sDim):
        """Deletes the given coordinated from the tree

        Args:
            point (tuple) : Coordinates to delete
            node (Node) : Entry node to start the search for the coordinates.
            sDim (Integer) : Splitting / cutting dimension of the input node

        Returns:
            Input node after deletion
        """

        if node is None:
            print('Empty tree!! Nothing to delete')
        elif point == node.point:
            if node.right:
                node.point = self.__find_min(node.right, sDim,
                        (sDim + 1) % self.dims)
                node.right = self.__delete(node.point, node.right,
                        (sDim + 1) % self.dims)
            elif node.left:
                node.point = self.__find_min(node.left, sDim,
                        (sDim + 1) % self.dims)
                node.right = self.__delete(node.point, node.left,
                        (sDim + 1) % self.dims)
                node.left = None
            else:
                node = None
        elif point[sDim] < node.point[sDim]:
            node.left = self.__delete(point, node.left, (sDim + 1) % self.dims)
        else:
            node.right = self.__delete(point, node.right,
                    (sDim + 1) % self.dims)

        return node

    def __traverse_inorder(self, node):
        """Traverse the tree in-order

        Args:
            node (Node) : Entry node to start traversal

        Returns:
            List of nodes in-order
        """

        if node is not None:
            n_list = []
            n_list.extend(self.__traverse_inorder(node.left))
            n_list.append(node.point)
            n_list.extend(self.__traverse_inorder(node.right))
            return n_list
        else:
            return []

    def __print_tree(self, node, sign=''):
        """Prints the tree in pre-order

        Args:
        node (Node) : An Entry node to start traversal
        sign (Character) : '-' indicates left sub-tree, '+' indicates right
                           sub-tree
        """

        if node is not None:
            print(sign, root.point)
            self.__print_tree(node.left, sign + '-')
            self.__print_tree(node.right, sign + '+')

    def insert(self, point):
        """Inserts the given point to the tree
        Args:
            point (tuple) : coordinates to insert to the tree
        """

        assert isinstance(point, tuple), 'Argument1 not a tuple'
        assert (len(point) == self.dims), \
                'Tuple should be of size {}'.format(self.dims)
        try:
            self.root = self.__insert(point, self.root, 0)
        except AttributeError:
            self.root = self.__insert(point, None, 0)

    def find_min(self, dim):
        """ Finds node with minimum value in the tree for the given dimension

        Args:
            dim (Integer) : Dimension to search for the minimum
        """

        try:
            return self.__find_min(self.root, dim, 0)
        except AttributeError:
            raise AttributeError('Empty Tree!!')

    def delete(self, point, sDim):
        """Deletes the node with the given point

        Args:
            point (tuple) : Point to search in the tree
            sDim (Integer) : Splitting / cutting dimension of the input node
        """

        assert isinstance(point, tuple), 'Argument1 not a tuple'
        assert (len(point) == self.dims), \
                'Argument1 should be of length {}'.format(self.dims)
        try:
            self.root = self.__delete(point, self.root, sDim)
        except AttributeError:
            raise AttributeError('Empty Tree!! Cannot Delete!!')

    def traverse_inorder(self):
        """Traverse the tree in-order

        Returns:
            List of values in the tree nodes
        """
        n_list = self.__traverse_inorder(self.root)
        return n_list

    def print_tree(self):
        """Prints the tree in pre-order """
        try:
            self.__print_tree(self.root)
        except AttributeError:
            print('Empty tree !!')

class TestNode(unittest.TestCase):

    def test_node_point(self):
        n1 = Node((8, 6))
        self.assertEqual(n1.point, (8, 6), 'Incorrect initialization')

    def test_node_left(self):
        n1 = Node((8, 6))
        n2 = Node((10, 1), n1)
        self.assertEqual(n2.left, n1, 'Incorrect initialization')

    def test_no_node_left(self):
        n1 = Node((8, 6))
        n2 = Node((10, 1), None, n1)
        try:
            n2.left
        except AttributeError:
            self.assertEqual(True, True, 'Should be an AttributeError')

    def test_node_right(self):
        n1 = Node((8, 6))
        n2 = Node((10, 1), None, n1)
        self.assertEqual(n2.right, n1, 'Incorrect initialization')

    def test_no_node_right(self):
        n1 = Node((8, 6))
        n2 = Node((10, 1), n1, None)
        try:
            n2.right
        except AttributeError:
            self.assertEqual(True, True, 'Should be an AttributeError')

class TestKDTree(unittest.TestCase):


    def test_insert(self):
        kdTree = KDTree(2)
        kdTree.insert((8, 6))
        kdTree.insert((10, 1))
        kdTree.insert((5, 8))
        kdTree.insert((9, 7))
        kdTree.insert((2, 1))
        kdTree.insert((3, 5))
        kdTree.insert((1, 7))
        kdTree.insert((7, 10))
        kdTree.insert((2, 9))
        kdTree.insert((6, 2))
        expect_list = [(1, 7), (2, 1), (6, 2), (3, 5), (5, 8), (2, 9), (7, 10),
                (8, 6), (10, 1), (9, 7)]
        actual_list = kdTree.traverse_inorder()
        self.assertEqual(expect_list, actual_list,
                'Algorithmic error!! \n expected: {} \n actual: {}'.format(
                    expect_list, actual_list))

    def test_findMin(self):
        kdTree = KDTree(2)
        kdTree.insert((8, 6))
        kdTree.insert((10, 1))
        kdTree.insert((5, 8))
        kdTree.insert((9, 7))
        kdTree.insert((2, 1))
        kdTree.insert((3, 5))
        kdTree.insert((1, 7))
        kdTree.insert((7, 10))
        kdTree.insert((2, 9))
        kdTree.insert((6, 2))

        xmin = kdTree.find_min(0)
        ymin = kdTree.find_min(1)
        self.assertEqual(xmin[0], 1, "Expected 1")
        self.assertEqual(ymin[1], 1, "Expected 1")

    def test_delete_rightSubTree(self):
        kdTree = KDTree(2)
        kdTree.insert((8, 6))
        kdTree.insert((10, 1))
        kdTree.insert((5, 8))
        kdTree.insert((9, 7))
        kdTree.insert((2, 1))
        kdTree.insert((3, 5))
        kdTree.insert((1, 7))
        kdTree.insert((7, 10))
        kdTree.insert((2, 9))
        kdTree.insert((6, 2))

        kdTree.delete((8, 6), 0)
        expect_list = [(1, 7), (2, 1), (6, 2), (3, 5), (5, 8), (2, 9), (7, 10),
                (9, 7), (10, 1)]
        actual_list = kdTree.traverse_inorder()
        self.assertEqual(expect_list, actual_list,
                'Algorithmic error!! \n expected: {} \n actual: {}'.format(
                    expect_list, actual_list))

    def test_delete_noRightSubTree(self):
        kdTree = KDTree(2)
        kdTree.insert((8, 6))
        kdTree.insert((5, 8))
        kdTree.insert((2, 1))
        kdTree.insert((3, 5))
        kdTree.insert((1, 7))
        kdTree.insert((7, 10))
        kdTree.insert((2, 9))
        kdTree.insert((6, 2))

        kdTree.delete((8, 6), 0)
        expect_list = [(1, 7), (2, 1), (6, 2), (3, 5), (5, 8), (2, 9), (7, 10)]
        actual_list = kdTree.traverse_inorder()
        self.assertEqual(expect_list, actual_list,
                'Algorithmic error!! \n expected: {} \n actual: {}'.format(
                    expect_list, actual_list))

if __name__ == "__main__" :

    unittest.main()
